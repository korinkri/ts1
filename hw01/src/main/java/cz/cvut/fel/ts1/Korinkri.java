package cz.cvut.fel.ts1;

public class Korinkri {
    public long factorial(int n) {
        if (n < 0) {
            return 0;
        } else if (n > 1) {
            return n * factorial(n-1);
        } else {
            return 1;
        }
    }
}
