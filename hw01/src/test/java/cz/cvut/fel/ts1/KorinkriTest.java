package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class KorinkriTest {

    public static Korinkri factorial;

    @BeforeAll
    public static void setUp() {
        factorial = new Korinkri();
    }

    @Test
    public void factorial_positiveNumberPassed_returnsCorrectResult() {
        // arrange
        int n = 4;
        long expectedResult = 24;

        // act
        long result = factorial.factorial(n);

        // assert
        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    public void factorial_zeroPassed_returnsOne() {
        // arrange
        int n = 0;
        long expectedResult = 1;

        // act
        long result = factorial.factorial(n);

        // assert
        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    public void factorial_negativeNumberPassed_returnsZero() {
        // arrange
        int n = -5;
        long expectedResult = 0;

        // act
        long result = factorial.factorial(n);

        // assert
        Assertions.assertEquals(expectedResult, result);
    }
}
