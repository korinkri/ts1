package cz.cvut.fel.ts1;

import org.junit.jupiter.api.*;

public class CalculatorTests {
    Calculator calculator;

    @BeforeAll
    private static void init() {

    }

    @BeforeEach
    public void setup() {
        calculator = new Calculator();
    }

    @Test
    public void Add_TwoPlusFour_Six() {
        int a = 2;
        int b = 4;
        int expectedResult = 6;

        int result = calculator.add(a, b);

        Assertions.assertEquals(expectedResult, result);

    }

    @Test
    public void Subtract_NegativeTwoMinusFour_NegativeSix() {
        int a = -2;
        int b = 4;
        int expectedResult = -6;

        int result = calculator.subtract(a, b);

        Assertions.assertEquals(expectedResult, result);

    }

    @Test
    public void Multiply_NegativeTwoTimesFour_NegativeEight() {
        int a = -2;
        int b = 4;
        int expectedResult = -8;

        int result = calculator.multiply(a, b);

        Assertions.assertEquals(expectedResult, result);

    }

    @Test
    public void Divide_NegativeTwoDividedByTwo_NegativeOne() {
        int a = -2;
        int b = 2;
        int expectedResult = -1;

        int result = calculator.divide(a, b);

        Assertions.assertEquals(expectedResult, result);

    }

    @Test
    public void Divide_TwoDividedByZero_ExceptionThrow() {
        int a = 2;
        int b = 0;

        Assertions.assertThrows(ArithmeticException.class, () -> calculator.divide(a, b));

    }

    @AfterEach
    public void CloseEach() {

    }

    @AfterAll
    public static void CloseAll() {

    }
}
